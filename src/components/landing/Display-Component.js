import React from 'react';
import { Container, Row, Col, Table } from 'react-bootstrap';
import '../../component-stylesheets/display-component.css';

const DisplayComponent = (props) => {
  return (
    <>
      <Container fluid className="displaybase  px-0" id="mainContent">
        <Col xs={10} className="mx-auto p-0">
          <img
            className="mt-5"
            src="/images/terminal1.png"
            alt="terminal-background"
            id="terminal-background"
          />
          <div>
            <Row className="mx-auto" id="terminal-content">
              <div>Displaying first 25 of 100 entries</div>
              <Table responsive>
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Author</th>
                    <th>Title</th>
                    <th>Categories</th>
                    <th>External url 🔗</th>
                  </tr>
                </thead>
                <tbody>
                  {props.posts.map((itm) => {
                    return (
                      <tr key={itm.ID}>
                        <td>{itm.ID}</td>
                        <td>
                          <img
                            className="d-block mx-auto rounded-circle"
                            src={itm.author.avatar_URL}
                            alt={itm.ID}
                          />
                          <span className="d-block">{itm.author.name}</span>
                        </td>
                        <td>{itm.title}</td>
                        <td>{Object.keys(itm.categories).toString()}</td>
                        <td>
                          <a
                            className="posts-link"
                            href={itm.shortURL}
                            onClick={() => window.open(itm.shortURL, '_blank')}
                          >
                            {itm.short_URL}
                          </a>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
                <span className={props.posts.length ? 'd-none' : 'block'}> Loading ... </span>
              </Table>
              <Row className="mx-auto">
                <span id="terminal-nudge" onClick={() => this.posts(props.start, props.end)}>
                  {'<'}
                </span>
                &nbsp;&nbsp;
                <span id="terminal-nudge" onClick={() => this.posts(props.start, props.end)}>
                  {'>'}
                </span>
              </Row>
            </Row>
          </div>
        </Col>
      </Container>
    </>
  );
};

export default DisplayComponent;
