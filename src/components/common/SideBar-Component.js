import React from 'react';
import '../../component-stylesheets/sidebar-component.css';
import { Container, Row, Table } from 'react-bootstrap';

const SideBarComponent = (props) => {
  console.log(props.cats);
  return (
    <Container>
      <div className={props.display === true ? 'd-none' : 'block sidebar '}>
        <Row onClick={() => props.closeDis()}>
          <span className="ml-auto mr-5 back_arrow_sidebar">
            <img src="/images/icons8-left-arrow-64.png" alt="close_sidenav" />
          </span>
        </Row>
        <Row className="top-categories text-dark ">
          <Table className="text-white">
            <thead>
              <tr>
                <th>📊 Top Categories:</th>
                <th>Posts:</th>
              </tr>
            </thead>
            <tbody>
              {props.cats.map((c) => {
                return (
                  <tr key={c.ID} className="text-left">
                    <td>
                      <a href={c.feed_url}>{c.name}</a>
                    </td>
                    <td>
                      <span className="badge badge-primary badge-pill">{c.post_count}</span>
                    </td>
                  </tr>
                );
              })}
              <span className={props.cats && props.cats.length ? 'd-none' : 'block'}>
                {' '}
                Loading ...{' '}
              </span>
            </tbody>
          </Table>
        </Row>
        <hr />
        <Row className="top-categories text-dark ">
          <Table className="text-white">
            <thead>
              <tr>
                <th>📈 Top Tags:</th>
                <th>Related:</th>
              </tr>
            </thead>
            <tbody>
              {props.tags.map((t) => {
                return (
                  <tr key={t.ID} className="text-left">
                    <td>
                      <a href={t.feed_url}>{t.name}</a>
                    </td>
                    <td>
                      <span className="badge badge-primary badge-pill">{t.post_count}</span>
                    </td>
                  </tr>
                );
              })}
              <span className={props.cats && props.cats.length ? 'd-none' : 'block'}>
                {' '}
                Loading ...{' '}
              </span>
            </tbody>
          </Table>
        </Row>
      </div>
    </Container>
  );
};

export default SideBarComponent;
