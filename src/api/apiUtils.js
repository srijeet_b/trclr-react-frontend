import axios from 'axios';
const SITE_URL = 'https://vibrant-blackwell-647bea.azurewebsites.net/api/blogs';
const SITE_CATS = 'https://vibrant-blackwell-647bea.azurewebsites.net/api/blogs/categories';
const SITE_TAGS = 'https://vibrant-blackwell-647bea.azurewebsites.net/api/blogs/tags';
// const SITE_URL = 'localhost:3000';

const apiUtils = async (start = 0, end = 25) => {
  let result = await axios.get(`${SITE_URL}/${start}/${end}`);
  if (result.status !== 200 || result.data.length === 0) {
    return [];
  }
  return result.data;
};

const getCats = async () => {
  let categories = await axios.get(SITE_CATS);
  if (categories.status !== 200 || categories.data.length === 0) {
    return [];
  }
  return categories.data;
};

const getTags = async () => {
  let tags = await axios.get(SITE_TAGS);
  if (tags.status !== 200 || tags.data.length === 0) {
    return [];
  }
  return tags.data;
};
export { apiUtils, getCats, getTags };
