import React from 'react';
import '../../component-stylesheets/wallpaper-component.css';
import '../../fonts/Barmeno_Regular.ttf';
import { Container } from 'react-bootstrap';

const WallPaperComponent = () => {
  return (
    <Container fluid id="wallpapercomp" className="p-0 m-0">
      <Container id="wallpaper-logo-area">
        <div>
          {/* <img src="/images/TrueCaller_Logo.png" alt="truecaller_logo" id="logo" /> */}
          <h1 className="text-lowercase"><span className="font-italic mr-2 true">True</span>caller</h1>
          <h3>Get the latest updates on our blog here !</h3>
        </div>
      </Container>
      <Container id="wallpaper-arrow-area" href="#mainContent">
        <div>
          <img
            src="/images/down-arrow.png"
            className="animated fadeOutDown"
            alt="down_Arrow"
            id="down_arrow"
          />
        </div>
      </Container>
    </Container>
  );
};

export default WallPaperComponent;
